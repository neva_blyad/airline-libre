"    airline_libre_Alt.vim
"    Copyright (C) 2019-2021 НЕВСКИЙ БЛЯДИНА <neva_blyad@lovecry.pt>
"                                            <neva_blyad@lovecri.es>
"
"    This file is part of airline-libre.
"
"    airline-libre is free software: you can redistribute it and/or modify
"    it under the terms of the GNU General Public License as published by
"    the Free Software Foundation, either version 3 of the License, or
"    (at your option) any later version.
"
"    airline-libre is distributed in the hope that it will be useful,
"    but WITHOUT ANY WARRANTY; without even the implied warranty of
"    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
"    GNU General Public License for more details.
"
"    You should have received a copy of the GNU General Public License
"    along with airline-libre.  If not, see <https://www.gnu.org/licenses/>.

scriptencoding utf-8

let s:DarkGreen   = ["#AFFF00", "#005F00", 154,  22, ""]
let s:Grey7       = ["#BCBCBC", "#121212", 250, 233, ""]
let s:Grey15      = ["#BCBCBC", "#262626", 250, 235, ""]
let s:Grey23      = ["#BCBCBC", "#3A3A3A", 250, 237, ""]
let s:Grey35      = ["#BCBCBC", "#585858", 250, 240, ""]
let s:Grey74      = ["#000000", "#BCBCBC",  16, 250, ""]
let s:GreenYellow = ["#005F00", "#AFFF00",  22, 154, ""]
let s:Red1        = ["#FFFFFF", "#FF0000", 231, 196, ""]

let g:airline#themes#airline_libre_Alt#palette = {}

" NORMAL
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

let s:airline_a_normal = s:GreenYellow
let s:airline_b_normal = s:Grey15
let s:airline_c_normal = s:Grey35

let g:airline#themes#airline_libre_Alt#palette.normal = airline#themes#generate_color_map(s:airline_a_normal, s:airline_b_normal, s:airline_c_normal)
let g:airline#themes#airline_libre_Alt#palette.normal.airline_warning = s:Red1

let g:airline#themes#airline_libre_Alt#palette.normal_modified =
\{
\   "airline_c": s:Grey74
\}
let g:airline#themes#airline_libre_Alt#palette.normal_modified.airline_warning = s:Red1

" INSERT
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

let s:airline_a_insert = s:DarkGreen
let s:airline_b_insert = s:GreenYellow
let s:airline_c_insert = s:GreenYellow

let g:airline#themes#airline_libre_Alt#palette.insert = airline#themes#generate_color_map(s:airline_a_insert, s:airline_b_insert, s:airline_c_insert)
let g:airline#themes#airline_libre_Alt#palette.insert.airline_warning = g:airline#themes#airline_libre_Alt#palette.normal.airline_warning

let g:airline#themes#airline_libre_Alt#palette.insert_modified =
\{
\   "airline_c": s:GreenYellow
\}
let g:airline#themes#airline_libre_Alt#palette.insert_modified.airline_warning = g:airline#themes#airline_libre_Alt#palette.normal.airline_warning

" REPLACE
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

let g:airline#themes#airline_libre_Alt#palette.replace = copy(g:airline#themes#airline_libre_Alt#palette.insert)
let g:airline#themes#airline_libre_Alt#palette.replace.airline_warning = g:airline#themes#airline_libre_Alt#palette.insert.airline_warning

let g:airline#themes#airline_libre_Alt#palette.replace_modified = g:airline#themes#airline_libre_Alt#palette.insert_modified
let g:airline#themes#airline_libre_Alt#palette.replace_modified.airline_warning = g:airline#themes#airline_libre_Alt#palette.insert.airline_warning

" VISUAL
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

let g:airline#themes#airline_libre_Alt#palette.visual = copy(g:airline#themes#airline_libre_Alt#palette.normal)
let g:airline#themes#airline_libre_Alt#palette.visual.airline_warning = g:airline#themes#airline_libre_Alt#palette.normal.airline_warning

let g:airline#themes#airline_libre_Alt#palette.visual_modified = g:airline#themes#airline_libre_Alt#palette.normal_modified
let g:airline#themes#airline_libre_Alt#palette.visual_modified.airline_warning = g:airline#themes#airline_libre_Alt#palette.normal.airline_warning

" Inactive buffer
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

let s:airline_a_inactive = s:Grey7
let s:airline_b_inactive = s:Grey15
let s:airline_c_inactive = s:Grey23

let g:airline#themes#airline_libre_Alt#palette.inactive = airline#themes#generate_color_map(s:airline_a_inactive, s:airline_b_inactive, s:airline_c_inactive)

" tabline extension
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

let g:airline#themes#airline_libre_Alt#palette.tabline =
\{
\   "airline_tablabel":           s:Grey15,
\   "airline_tab":                s:Grey15,
\   "airline_tabsel":             s:GreenYellow,
\   "airline_tabtype":            s:GreenYellow,
\   "airline_tabfill":            s:Grey35,
\   "airline_tabmod":             s:GreenYellow,
\   "airline_tabhid":             s:Grey35,
\   "airline_tabmod_unsel":       s:Grey74,
\
\   "airline_tabsel_right":       s:GreenYellow,
\   "airline_tab_right":          s:Grey35,
\   "airline_tabmod_right":       s:GreenYellow,
\   "airline_tabhid_right":       s:Grey35,
\   "airline_tabmod_unsel_right": s:Grey74
\}
